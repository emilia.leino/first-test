const expect = require("chai").expect
const assert = require("chai").assert
const should = require("chai").should()
const mylib = require("../src/mylib")

describe("Unit testing mylib.js", () => {
    let myvar = undefined
    before(() => {
        myvar = 1
    })

    it("Should return 2 when using sum function with a=1 and b=1", () => {
        const result = mylib.sum(1,1)
        expect(result).to.equal(2)
    })
    it("Should return 8 when using expo function with a=2 and b=3", () => {
        const result = mylib.expo(2,3)
        expect(result).to.equal(8)
    })
    it("Should return 2 when using modulo function with a=8 and b=3", () => {
        const result = mylib.modulo(8,3)
        assert(result === 2)
    })
    
    it.skip("Assert foo is not bar", () => {
        assert("foo" !== "bar")
    })
    it("myvar should exist", () => {
        should.exist(myvar)
    })

    after(() => {
        console.log("hello")
    })
})